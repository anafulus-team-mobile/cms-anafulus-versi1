<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <meta name="csrf-token">

  <title>AiChat CMS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ url('/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ url('/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <!-- external css plugin -->
  @yield('css-plugin')
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ url('/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ url('/css/style.css?v='.rand()) }}">
  <!-- page style -->
  @yield('css')

  <link rel="canonical shortcut icon" href="{{ url('/favicon.ico')}}" />
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>AC</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>A</b>nafulus CMS</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    @include('layouts._notification')

  </header>
  <!-- Left side column. contains the logo and sidebar -->
  @include('layouts._menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @yield('header-title')
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        @yield('breadcrumb')
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        @yield('content')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="{{ url('/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ url('/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ url('/plugins/fastclick/fastclick.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ url('/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- js.cookie -->
<script src="{{ url('/plugins/jscookie/js.cookie.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ url('/js/admin-lte-app.js?v='.rand()) }}"></script>
<!--<script src="{{ url('/js/script.js') }}"></script>-->
<!-- page script -->
<script>
</script>

@yield('js')

</body>
</html>