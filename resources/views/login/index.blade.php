@extends('layouts.simple')
@section('css')
<!-- iCheck -->
  <link rel="stylesheet" href="{{ url('/plugins/iCheck/square/blue.css') }}">
@endsection

@section('content')
<div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="{{ url('/login') }}" method="post">
        <div class="form-group {{ $errors->has('password') ? ' has-error' : ' has-feedback' }}">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <span class="help-block">
                    <strong></strong>
                </span>
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : ' has-feedback' }}">
            <input id="password" type="password" class="form-control" name="password" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <span class="help-block">
                    <strong></strong>
                </span>
          </div>
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox" name="remember"> Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
    </form>

    <a href="{{ url('/password/reset') }}">Forgot Your Password?</a><br>

</div>
@endsection

@section('js')
<!-- iCheck -->
<script src="{{ url('/plugins/iCheck/icheck.min.js') }}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
@endsection