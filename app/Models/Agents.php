<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Agents extends Model
{
	protected $table = 'm_agents';

    protected $fillable = [
        'id_agent',
        'agent_local_id',
        'agent_family_card_id',
        'agent_category',
        'active_status'
    ];
}

