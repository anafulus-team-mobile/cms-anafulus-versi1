<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Borrowers extends Model
{
	protected $table = 'm_borrowers';

    protected $fillable = [
        'id_borrower',
        'borrower_local_id',
        'borrower_family_card_id'

    ];
}

