<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Landers extends Model
{
	protected $table = 'm_lenders';

    protected $fillable = [
        'id_lender',
        'lender_local_id',
        'company_name',
        'company_phone_number'

    ];
}

