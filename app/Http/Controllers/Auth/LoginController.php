<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Admins;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $email=$request->email;
        $password=$request->password;
        $query=Admins::where('email','=',$email)->where('password','=',$password)->get();
        return $query;
    }
}
