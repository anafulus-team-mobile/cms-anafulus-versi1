<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDAgents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_agents', function (Blueprint $table) {
            $table->bigIncrements('id_detail_agent');
            $table->bigInteger('id_agent')->unsigned();
            $table->bigInteger('id_province')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->integer('phone_number');
            $table->string('image');
            $table->string('password');
            $table->timestamps();
            $table->index('id_agent', 'FK_d_agents_agents');
            $table->index('id_province', 'FK_d_agents_provinces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_agents');
    }
}
