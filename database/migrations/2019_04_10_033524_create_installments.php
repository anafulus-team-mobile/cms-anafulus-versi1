<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstallments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installments', function (Blueprint $table) {
            $table->bigIncrements('id_installment');
            $table->bigInteger('id_loan')->unsigned();
            $table->date('due_date');
            $table->date('payment_date');
            $table->integer('installment_order');
            $table->boolean('installment_status');
            $table->timestamps();
            $table->index('id_loan', 'FK_installments_loans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installments');
    }
}
