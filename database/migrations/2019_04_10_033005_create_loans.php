<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->bigIncrements('id_loan');
            $table->bigInteger('id_borrower')->unsigned();
            $table->bigInteger('id_agent')->unsigned();
            $table->string('loan_category');
            $table->string('loan_type');
            $table->string('name_of_responsible');
            $table->integer('loan_principal');
            $table->integer('loan_approved');
            $table->date('submission_date');
            $table->date('approved_date');
            $table->string('tenor');
            $table->integer('total_borrower');
            $table->string('loan_purpose');
            $table->boolean('loan_status');
            $table->integer('installment_nominal');
            $table->string('loan_files_submission');
            $table->timestamps();

            $table->index('id_borrower', 'FK_loans_borrowers');
            $table->index('id_agent', 'FK_loans_agents');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
