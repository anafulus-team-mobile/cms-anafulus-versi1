<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulaLoans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formula_loans', function (Blueprint $table) {
            $table->bigIncrements('id_formula_loan');
            $table->bigInteger('id_loan')->unsigned();
            $table->float('interest_rate');
            $table->float('margin_rate');
            $table->integer('weeks_in_year');
            $table->string('description');
            $table->string('formula');
            $table->timestamps();
            $table->index('id_loan', 'FK_loans_formula_loans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formula_loans');
    }
}
