<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMAgents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_agents', function (Blueprint $table) {
            $table->bigIncrements('id_agent');
            $table->integer('agent_local_id');
            $table->integer('agent_family_card_id');
            $table->date('birth_date');
            $table->string('gender');
            $table->string('agent_category');
            $table->string('active_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_agents');
    }
}
