<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulaTaxes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formula_taxes', function (Blueprint $table) {
            $table->bigIncrements('id_formula_tax');
            $table->string('tax_category');
            $table->float('interest_rate');
            $table->float('tax_percentage');
            $table->integer('weeks_in_year');
            $table->string('description');
            $table->string('formula');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formula_taxes');
    }
}
