<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDLenders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_lenders', function (Blueprint $table) {
            $table->bigIncrements('id_detail_lender');
            $table->bigInteger('id_lender')->unsigned();
            $table->bigInteger('id_province')->unsigned();
            $table->string('name_of_responsible');
            $table->integer('lender_fund');
            $table->timestamps();
            $table->index('id_lender', 'FK_d_lenders_lenders');
            $table->index('id_province', 'FK_d_lenders_provinces');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_lenders');
    }
}
