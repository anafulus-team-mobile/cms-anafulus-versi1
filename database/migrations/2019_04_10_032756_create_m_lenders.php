<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMLenders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_lenders', function (Blueprint $table) {
            $table->bigIncrements('id_lender');
            $table->string('company_name',50);
            $table->integer('lender_local_id');
            $table->integer('company_tax_id');
            $table->string('company_email');
            $table->integer('company_phone_number');
            $table->boolean('groos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_lenders');
    }
}
