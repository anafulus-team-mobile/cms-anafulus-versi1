<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDBorrowers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_borrowers', function (Blueprint $table) {
            $table->bigIncrements('id_detail_borrower');
            $table->bigInteger('id_borrower')->unsigned();
            $table->bigInteger('id_province')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->integer('phone_number');
            $table->string('last_job');
            $table->integer('income');
            $table->string('image');
            $table->string('password');
            $table->float('balance');
            $table->timestamps();
            $table->index('id_borrower', 'FK_d_borrowers_borrowers');
            $table->index('id_province', 'FK_d_borrowers_provinces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_borrowers');
    }
}
