<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusAgents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_agents', function (Blueprint $table) {
            $table->bigIncrements('id_bonus_agent');
            $table->bigInteger('id_agent')->unsigned();
            $table->string('bonus_category');
            $table->string('bonus_description');
            $table->timestamps();
            $table->index('id_agent', 'FK_bonus_agents_agents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonus_agents');
    }
}
