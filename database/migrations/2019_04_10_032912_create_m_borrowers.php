<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMBorrowers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_borrowers', function (Blueprint $table) {
            $table->bigIncrements('id_borrower');
            $table->integer('id_facebook');
            $table->integer('id_instagram');
            $table->integer('borrower_local_id');
            $table->integer('borrower_family_card_id');
            $table->date('birth_date');
            $table->string('gender');
            $table->integer('bank_acc_number');
            $table->string('last_education');
            $table->string('active_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_borrowers');
    }
}
