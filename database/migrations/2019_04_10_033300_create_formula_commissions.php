<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulaCommissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formula_commissions', function (Blueprint $table) {
            $table->bigIncrements('id_formula_commission');
            $table->string('commission_category');
            $table->string('step');
            $table->float('commission_percentage');
            $table->float('admin_percentage');
            $table->integer('weeks_in_year');
            $table->string('description');
            $table->string('formula');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formula_commissions');
    }
}
